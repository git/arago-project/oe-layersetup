
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

If you have not set MACHINE in your local.conf you can run
'MACHINE=xxxxx bitbake <target>'

Common targets are:
    tisdk-default-image
    tisdk-base-image
    tisdk-bootstrap-base-image
    tisdk-bootstrap-image
    tisdk-thinlinux-image
    tisdk-tiny-image

You can also run generated qemu images with a command like 'runqemu qemux86-64'.

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
