# This file takes repo entries in the format
# repo name,repo uri,repo branch,repo commit[,layers=layer1:layer2...:layern]

bitbake,https://git.openembedded.org/bitbake,2.8,HEAD
meta-arago,https://git.yoctoproject.org/meta-arago,scarthgap,HEAD,layers=meta-arago-distro:meta-arago-extras:meta-arago-test
#meta-browser,https://github.com/OSSystems/meta-browser.git,master,68e8cbf51ac46c8c1b3923ee2b20e27ba9d3faa4,layers=meta-chromium
#meta-selinux,https://git.yoctoproject.org/meta-selinux,scarthgap,HEAD,layers=
meta-qt6,https://code.qt.io/yocto/meta-qt6.git,6.9,HEAD,layers=
meta-virtualization,https://git.yoctoproject.org/meta-virtualization,scarthgap,HEAD,layers=
meta-openembedded,https://git.openembedded.org/meta-openembedded,scarthgap,HEAD,layers=meta-networking:meta-python:meta-oe:meta-gnome:meta-filesystems:meta-multimedia
meta-ti,https://git.yoctoproject.org/meta-ti,scarthgap,HEAD,layers=meta-ti-extras:meta-beagle:meta-ti-bsp
meta-arm,https://git.yoctoproject.org/meta-arm,scarthgap,HEAD,layers=meta-arm:meta-arm-toolchain
meta-clang,https://github.com/kraj/meta-clang,scarthgap,HEAD,layers=
oe-core,https://git.openembedded.org/openembedded-core,scarthgap,HEAD,layers=meta

OECORELAYERCONF=./sample-files/bblayers.conf.sample
OECORELOCALCONF=./sample-files/local-arago64-v2.conf.sample
BITBAKE_INCLUSIVE_VARS=yes
